import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Demon slayer c une dinguerie')
    .setDescription('This is the test et ça fonctionne !!! :)')
    .setVersion('0.1')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('v1/user/doc', app, document);

  app.enableCors();
  await app.listen(process.env.APP_PORT ?? 3005, '0.0.0.0');
}
bootstrap();
